"""
# Definition for a Node.
class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children
"""

class Solution:
    def preorder(self, root: 'Node') -> List[int]:
        preord_list = []
        
        if not root: return None
        
        def recursion_helper(root, preord_list):
            preord_list.append(root.val)
            
            if root.children:
                for child in root.children:
                    helper(child, preord_list)
                    
            return preord_list
                    
        return recursion_helper(root, preord_list)