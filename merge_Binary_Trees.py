# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def mergeTrees(self, t1: TreeNode, t2: TreeNode) -> TreeNode:
        """
        if not t1.head and not t2.head:
            return None
        else:
            Start at Head -> Return L.head.val + R.head.val 
            head.left = mergeTrees(t1.left, t2.left)
            head.right = mergeTrees(t1.right, t2.right)
        """
        if not t1 and not t2:
            return None
        elif t1 and not t2:
            return t1
        elif t2 and not t1: 
            return t2
        else: 
            root = TreeNode(t1.val + t2.val)
            root.left = self.mergeTrees(t1.left, t2.left)
            root.right = self.mergeTrees(t1.right, t2.right)
            return root
     
        return root