class node():
    def __init__(self, node_val, next = None):
        self.val = node_val
        self.next = next

class singleLinkedList():
    def __init__(self):
        self.head = None
        self.tail = None 

    def add(self, node_val):
        if not self.head:
            self.tail = self.head = node(node_val)
            return
        self.tail.next = node(node_val)
        self.tail = self.tail.next

    def add_multiple(self, vals):
        for v in vals:
            self.add(v)

    def list_ll(self):
        node = self.head 
        ll_list = []
        while node:
            ll_list.append(str(node.val))
            node = node.next
        return ll_list

    def print_ll(self):
        print("{}".format('->'.join(self.list_ll())))

""""
sll1 = singleLinkedList()
sll1.add_multiple([1,2,3,4,5,6])
sll1.print_ll()
"""