def primes_below_n(n):
    num_arr = [1] * n       ## Create an Array for nums LESS THAN N.
    num_arr[0] = num_arr[1] = 0     ## 0 and 1 are NOT primes. So, set them to 0. 
    m = 2                      ## Start iteration at number 2. 

    for m in range(2, n):
        if num_arr[m] == 1: ## Only test its multiples if num_arr[m] is 1. 
            for j in range(m+m, n, m):    ## Only set MULTIPLES of m to Non-prime, not m itself. (For eg, 2 is still a prime, so, start at 2 * 2)
                if num_arr[j] == 1:     
                    num_arr[j]=0
    
    return num_arr.count(1)

print(primes_below_n(10))