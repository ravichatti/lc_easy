from itertools import cycle 
def roman_to_integer(roman):
    rtoi_dict = {'I':1, 'V':5, 'X':10, 'C':100, 'L':50, 'D': 500, 'M':1000}
    skip = False
    next_char = cycle(roman) 
    next(next_char)
    integer_val = 0
    for i, char in enumerate(roman):
        nxt = next(next_char) if i != len(roman) - 1 else None  ## This is needed to stop Cycle at End of Roman Character string. 
        print("Char is {}. Skip is {}. Next Char is {}".format(char, skip, nxt))
        if skip:
            skip = False
        elif char == 'I' and (nxt == 'X' or nxt == 'V'):
            integer_val += rtoi_dict[nxt] - rtoi_dict[char]
            skip = True
        elif char == 'X' and (nxt == 'L' or nxt == 'C'):
            integer_val += rtoi_dict[nxt] - rtoi_dict[char]
            skip = True
        elif char == 'C' and (nxt == 'D' or nxt == 'M'):
            integer_val += rtoi_dict[nxt] - rtoi_dict[char]
            skip = True
        else:
            integer_val += rtoi_dict[char]
            skip = False
        print("Integer Val is {}".format(integer_val))
    return integer_val

print(roman_to_integer('XXX'))
print(roman_to_integer('III'))
print(roman_to_integer('IV'))
print(roman_to_integer('LVIII'))
print(roman_to_integer('XCII'))
>>>>>>> 64732aa5631ea5a6e7cecfbf9aac6e715b119c56
