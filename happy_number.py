def happy_number(in_num):
    happy_list = []
    happy = in_num

    def helper(happy, happy_list):
        new_happy = sum([int(i) ** 2 for i in str(happy)])

        if new_happy in happy_list:
            return False 
        if new_happy == 1:
            return True

        happy_list.append(new_happy)
        return helper(new_happy, happy_list)
    return helper(happy, happy_list)


print(happy_number(92))
print(happy_number(82))
print(happy_number(19))