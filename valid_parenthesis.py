def valid_parenthesis(s):
        p_dict = {'{':'}', '[':']', '(':')'}
        p_stack = []
        for p in s:
            #print("P_DICT is {} and P_STACK is {}".format(p_dict, p_stack))
            
            ## If its an Opening Parenthesis, ALWAYS add to Stack
            if p in p_dict.keys():
                p_stack.append(p)
            
            ## If Closing and empty stack or doesn`t match stack_top, then not balanced
            elif p in p_dict.values() and (not p_stack or p != p_dict[p_stack[-1]]):
                return False
            
            ## If it closes last in Stack, then Pop Stack
            elif p == p_dict[p_stack[-1]]:
                p_stack.pop()

        return len(p_stack) == 0

print(valid_parenthesis('((()))'))
print(valid_parenthesis('()[]{}'))
print(valid_parenthesis('(]'))
print(valid_parenthesis('([)]'))
print(valid_parenthesis('{[]}'))
print(valid_parenthesis('{]}'))