from itertools import cycle 
def nonDecreasingArray(in_arr):
    diff_arr = []
    max_el = in_arr[0]
    for i in range(1, len(in_arr)):
        print("Max is {}".format(max_el))
        diff_arr.append(in_arr[i] - max_el)
        max_el = max(in_arr[i], max_el)
    return len([i for i in diff_arr if i < 0]) <= 1

print(nonDecreasingArray([3,4,2,3]))
print(nonDecreasingArray([4,2,3]))
#print(nonDecreasingArray([4,1,4,3]))
