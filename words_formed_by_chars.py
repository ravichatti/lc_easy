from collections import Counter

def words_formed_by_chars(words, chars):
    return sum([len(word) for word in words if len(Counter(chars) - Counter(word)) > 1 and len(Counter(word) - Counter(chars)) == 0])

print(words_formed_by_chars(["cat","bt","hat","tree"], "atach"))