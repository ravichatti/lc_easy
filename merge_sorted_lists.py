# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        outListHead = ListNode()
        outListNode = outListHead
        while l1 and l2:
            #print("L1 is {} and L2 is {}".format(l1.val, l2.val))
            if l2.val > l1.val:
                outListNode.next = ListNode(l1.val)
                outListNode = outListNode.next
                l1 = l1.next 
            else:
                outListNode.next = ListNode(l2.val)
                outListNode = outListNode.next
                l2 = l2.next

        while l1:
            outListNode.next = ListNode(l1.val)
            outListNode = outListNode.next
            l1 = l1.next 

        while l2:
            outListNode.next = ListNode(l2.val)
            outListNode = outListNode.next
            l2 = l2.next 

        return outListHead.next