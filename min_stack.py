"""
Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

push(x) -- Push element x onto stack.
pop() -- Removes the element on top of the stack.
top() -- Get the top element.
getMin() -- Retrieve the minimum element in the stack.
"""


"""
Input
["MinStack","push","push","push","getMin","pop","top","getMin"]
[[],[-2],[0],[-3],[],[],[],[]]

Output
[null,null,null,null,-3,null,0,-2]
"""
class MinStack(object):

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.array = []
        

    def push(self, x):
        """
        :type x: int
        :rtype: None
        """
        if not self.array:
            self.array.append((x,x))
        else:
            self.array.append((x, min(x, self.array[-1][1])))
        

    def pop(self):
        """
        :rtype: None
        """
        if not self.array:
            return None
        
        last_el = self.array.pop()
        return last_el[0]
        

    def top(self):
        """
        :rtype: int
        """
        if not self.array:
            return None
        return self.array[-1][0]
        

    def getMin(self):
        """
        :rtype: int
        """
        if not self.array:
            return None
        return self.array[-1][1]
        


# Your MinStack object will be instantiated and called as such:
# obj = MinStack()
# obj.push(x)
# obj.pop()
# param_3 = obj.top()
# param_4 = obj.getMin()

ms = min_stack()
# print(ms.getMin())
print(ms.push(10))
print(ms.push(20))
print(ms.push(30))
print(ms.push(40))
print(ms.array)

print(ms.getMin())
