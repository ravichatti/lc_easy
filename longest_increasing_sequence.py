def longest_inc_sequence(in_arr):
        """ Idea: 1 Pointer. If next element > prev: Counter + 1. """ 
        if len(in_arr) == 0: return 0
        max_count = 0 
        prev, local_count = in_arr[0], 0
        for num in in_arr:
            if num > prev:
                print("Num is {} and Prev is {}".format(num, prev))
                local_count += 1
                max_count = max(local_count, max_count)
                prev = num
            else:
                local_count = 0
                prev = num
        return max_count+1

print(longest_inc_sequence([1,2,3,5]))
print(longest_inc_sequence([1,2]))
print(longest_inc_sequence([3, 2, 1]))