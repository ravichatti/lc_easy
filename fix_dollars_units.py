import pandas as pd 

path = r'/data/Data_Cleaning/Data_Archives/2020/ptsns2'
in_file = path + 'PTS_SPINS_08242020063034.csv'
out_file = 'PTS_SPINS_08242020063034_EDITED.csv'

def flip_sign(in_val):
    if in_val[-1] != '-':
        return in_val 
    else:
        return '-' + in_val[:-1]

df = pd.read_csv(in_file)

df["Sales Units"] = df["Sales Units"].apply(flip_sign)
df['Sales$'] = df['Sales$'].apply(flip_sign)

df.to_csv(out_file, sep=',', header=True, index=False)