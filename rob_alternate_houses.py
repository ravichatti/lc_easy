def rob_alternate_houses(house_assets):
    #print("House Assets is {}".format(house_assets))
    if len(house_assets) == 1:
        return house_assets[0]
    elif len(house_assets) == 2:
        return max(house_assets)

    print("Current {}. Loot Current and 2+ houses: {}. Look Next House+ {}".format(house_assets[0], house_assets[0] + rob_alternate_houses(house_assets[2:]), rob_alternate_houses(house_assets[1:])))
    max_loot = max(house_assets[0] + rob_alternate_houses(house_assets[2:]), rob_alternate_houses(house_assets[1:]))

    return max_loot    

# print(rob_alternate_houses([10,40,12,2,14,16,19]))

print(rob_alternate_houses([183,219,57,193,94,233,202,154,65,240,97,234,100,249,186,66,90,238,168]))