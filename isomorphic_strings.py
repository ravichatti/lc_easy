def isomorphic_strings(s1, s2):
    return len(set(s1)) == len(set(s2)) == len(set([i for i in zip(s1, s2)]))


print(isomorphic_strings('add', 'egg'))
print(isomorphic_strings('foo', 'bar'))
print(isomorphic_strings('title', 'paper'))