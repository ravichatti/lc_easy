def factorial_trailing_zeros(n):
    count = 0
    while n != 0:
        n = n // 5
        count = count + n

        """ 
        n = 30, count = 0
        n = 5, count = 6
        n = 1, count = 7 
        n = 0, count = 7
        """
    return count 


print(factorial_trailing_zeros(5))
print(factorial_trailing_zeros(30))