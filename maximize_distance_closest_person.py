def maximize_distance(seats):
    """ 1 means occupied. 0 means Empty. Find an open spot that is farthest from other occupied seats"""

    dist_1, dist_2, dist_3 = 0,0,0

    if seats[0] == 0:
        for seat in seats:
            if not seat:
                dist_1 += 1
            else:
                break 

    if seats[-1] == 0:
        for j in range(len(seats)-1, 0, -1):
            if seats[j] == 0:
                dist_2 += 1
            else:
                break 

    local_max = 0
    for i in range(1, len(seats)):
        if seats[i] == 0 and seats[i-1] == 0:
            local_max += 1
            dist_3 = max(dist_3, local_max)
        else:
            local_max = 0
    
    return max(dist_1, dist_2, dist_3//2 + 1)
            

print(maximize_distance([1,0,0,0,0,1,1,0,0]))