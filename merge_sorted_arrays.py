def merge_sorted_arrays(nums1, m, nums2, n):
    """
    Idea: Iterate Nums 1 (i) and Nums 2 (j) in reverse. Pick Greater element and add to End of Nums1 (k). 
    """

    if m == 0:
        return nums2 
    elif n == 0:
        return nums1

    i = m -1
    j = n -1 
    k = m + n - 1

    print("I is {} J is {} K is {}".format(i,j, k))
    while i>=0 and j>=0:
        print("I is {} J is {} K is {}".format(i,j, k))
        if nums1[i] > nums2[j]:
            nums1[k] = nums1[i]
            i-=1
        else:
            nums1[k] = nums2[j]
            j-=1
        k-=1
    
    if j >= 0:
        print("I is {} J is {} K is {}".format(i,j, k))
        nums1[:k+1] = nums2[:j+1]

    return nums1

##print(merge_sorted_arrays([1,5,7,8,0,0,0,0], [4,4,6,7]))
print(merge_sorted_arrays([1,2,3,0,0,0], 3, [2,5,6], 3))

print(merge_sorted_arrays([0], 0, [1], 1))