# Definition for singly-linked list.
class ListNode:
     def __init__(self, x):
         self.val = x
         self.next = None

class Solution:
    def hasCycle(self, head):
        node = head 

        try:
            slow = node.next 
            fast = node.next.next 

            while slow != fast:
                slow = slow.next 
                fast = fast.next.next
            return True

        except:
            return False

sll1 = ListNode(3)
sll1.next = ListNode(2)
sll1.next.next = ListNode(0)
sll1.next.next.next = ListNode(-4)
sll1.next.next.next.next = sll1.next

sol = Solution()
print(sol.hasCycle(sll1))

sll2 = ListNode(1)
sll2.next = ListNode(2)
sll2.next.next = sll2
print(sol.hasCycle(sll2))