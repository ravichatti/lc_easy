def fibn(n):
    """ Method that returns the trailing number of Zeros """
    print("N is {}".format(n))
    if n == 0 or n == 1: return 1 

    fib_hash = {0:1, 1:1}

    fib_hash[n] = fibn(n-1) if (n-1) not in fib_hash else fib_hash[n-1] + fibn(n-2) if (n-2) not in fib_hash else fib_hash[n-2]
    print("Fibonacci {} is {}".format(n, fib_hash[n]))
    return fib_hash[n]

print(fibn(5))