def rotate(nums, k):
    """
        Do not return anything, modify nums in-place instead.
    """
        
    rotate_mod = k % len(nums)
    print("Rotate Mod is {}".format(rotate_mod))
    if rotate_mod == 0: 
        pass
    else:
        nums = nums[rotate_mod:] + nums[:rotate_mod] 
        print(nums)
    return nums 

print(rotate([1,2,3,4], 2))