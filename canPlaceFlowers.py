def canPlaceFlowers(flowerbed, n):
    flowerbed = [0] + flowerbed + [0]
    cnt = 0

    for i in range(len(flowerbed)):
        if flowerbed[i:i+3] == [0,0,0]:
            cnt += 1
            flowerbed[i+1] = 1
        if cnt >= n:
            return True

    return False

print(canPlaceFlowers([1,0,0,0,1], 1))
print(canPlaceFlowers([1,0,0,0,1], 2))
print(canPlaceFlowers([1,0,0,0,0,1], 2))
print(canPlaceFlowers([1,0,0,0,1,0,0], 2))