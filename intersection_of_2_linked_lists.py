# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def getIntersectionNode(self, headA, headB):
        pa = headA 
        pb = headB

        if pa is None or pb is None:
            return None
            
        while pa != pb:
                pa = headB if not pa else pa.next   
                pb = headA if not pb else pb.next

        return pa


sll1 = ListNode(4)
sll1.next = ListNode(1)

sll2 = ListNode(5)
sll2.next = ListNode(6)
sll2.next.next = ListNode(1)

sll3 = ListNode(3)
sll3.next = ListNode(4)
sll3.next.next = ListNode(5)

sll1.next.next = sll3
sll2.next.next.next = sll3

sol = Solution()
print(sol.getIntersectionNode(sll1, sll2).val)