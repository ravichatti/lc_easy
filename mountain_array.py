def peakIndexInMountainArray(arr):
    prev = arr[0]
    for num in range(1, len(arr)):
        if arr[num] > prev:
            prev = arr[num]
        else:
            return num-1

print(peakIndexInMountainArray([0,10,5,2]))

def find_peak_binary_search(arr):
    ## Find Peak in Mountain Array using Binary Search 
    l, r = 0, len(arr)
    while l < r: 
        mid = (l + r) // 2
        if arr[mid - 1] < arr[mid] and arr[mid] > arr[mid +1]:
            return mid
        elif arr[mid] > arr[mid-1]: ## Peak is in right 
            l = mid
        elif arr[mid] < arr[mid-1]: ## Peak is in left
            r = mid

print(find_peak_binary_search([0,10,5,2]))